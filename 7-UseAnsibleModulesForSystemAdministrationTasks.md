# Use Ansible modules for system administration tasks that work with:

<!-- vscode-markdown-toc -->

* [Software packages and repositories](#Softwarepackagesandrepositories)
* [Services](#Services)
* [Firewall rules](#Firewallrules)
* [File systems](#Filesystems)
* [Storage devices](#Storagedevices)
* [File content](#Filecontent)
* [Archiving](#Archiving)
* [Scheduled tasks](#Scheduledtasks)
* [Security](#Security)
* [Users and groups](#Usersandgroups)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## <a name='Softwarepackagesandrepositories'></a>Software packages and repositories

## <a name='Services'></a>Services

## <a name='Firewallrules'></a>Firewall rules

## <a name='Filesystems'></a>File systems

## <a name='Storagedevices'></a>Storage devices

## <a name='Filecontent'></a>File content

## <a name='Archiving'></a>Archiving

## <a name='Scheduledtasks'></a>Scheduled tasks

## <a name='Security'></a>Security

## <a name='Usersandgroups'></a>Users and groups
