# Use advanced Ansible features

<!-- vscode-markdown-toc -->

* [Create and use templates to create customized configuration files](#Createandusetemplatestocreatecustomizedconfigurationfiles)
* [Use Ansible Vault in playbooks to protect sensitive data](#UseAnsibleVaultinplaybookstoprotectsensitivedata)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## <a name='Createandusetemplatestocreatecustomizedconfigurationfiles'></a>Create and use templates to create customized configuration files

## <a name='UseAnsibleVaultinplaybookstoprotectsensitivedata'></a>Use Ansible Vault in playbooks to protect sensitive data

    > Create and use templates to create customized configuration files Work with Ansible variables and facts Create and work with roles Download roles from an Ansible Galaxy and use them Manage parallelism Use Ansible Vault in playbooks to protect sensitive data Use provided documentation to look up specific information about Ansible modules and commands
    