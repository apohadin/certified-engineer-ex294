# Configure Ansible managed nodes

<!-- vscode-markdown-toc -->

* [Create and distribute SSH keys to managed nodes](#CreateanddistributeSSHkeystomanagednodes)
* [Configure privilege escalation on managed nodes](#Configureprivilegeescalationonmanagednodes)
* [Validate a working configuration using ad hoc Ansible commands](#ValidateaworkingconfigurationusingadhocAnsiblecommands)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## <a name='CreateanddistributeSSHkeystomanagednodes'></a>Create and distribute SSH keys to managed nodes

## <a name='Configureprivilegeescalationonmanagednodes'></a>Configure privilege escalation on managed nodes

## <a name='ValidateaworkingconfigurationusingadhocAnsiblecommands'></a>Validate a working configuration using ad hoc Ansible commands
