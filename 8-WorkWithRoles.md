# Work with roles

<!-- vscode-markdown-toc -->

* [Create roles](#Createroles)
* [Download roles from an Ansible Galaxy and use them](#DownloadrolesfromanAnsibleGalaxyandusethem)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## <a name='Createroles'></a>Create roles

## <a name='DownloadrolesfromanAnsibleGalaxyandusethem'></a>Download roles from an Ansible Galaxy and use them
