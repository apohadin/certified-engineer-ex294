# Script administration tasks

<!-- vscode-markdown-toc -->

* [Create simple shell scripts](#Createsimpleshellscripts)
* [Create simple shell scripts that run ad hoc Ansible commands](#CreatesimpleshellscriptsthatrunadhocAnsiblecommands)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## <a name='Createsimpleshellscripts'></a>Create simple shell scripts

## <a name='CreatesimpleshellscriptsthatrunadhocAnsiblecommands'></a>Create simple shell scripts that run ad hoc Ansible commands
