# Red Hat Certified Engineer (EX294)

## Links

* [Red Hat Certified Engineer](https://www.redhat.com/en/services/certification/rhce)
* [Red Hat Certified Engineer (RHCE) exam for Red Hat Enterprise Linux 8](https://www.redhat.com/en/services/training/ex294-red-hat-certified-engineer-rhce-exam-red-hat-enterprise-linux-8)

## Person of Interest

## Table of Content

* [Be able to perform all tasks expected of a Red Hat Certified System Administrator](1-PerformAllTasksExpectedOfRHCSA.md)
* [Understand core components of Ansible](2-UnderstandCoreComponentsOfAnsible.md)
* [Install and configure an Ansible control node](3-InstallAndConfigureAnAnsibleControlNode.md)
* [Configure Ansible managed nodes](4-ConfigureAnsibleManagedNodes.md)
* [Script administration tasks](5-ScriptAdministrationTasks.md)
* [Create Ansible plays and playbooks](6-CreateAnsiblePlaysAndPlaybooks.md)
* [Work with roles](8-WorkWithRoles.md)
* [Use advanced Ansible features](9-UseAdvancedAnsibleFeatures.md)
