# Understand core components of Ansible

<!-- vscode-markdown-toc -->

* [Inventories](#Inventories)
* [Modules](#Modules)
* [Variables](#Variables)
* [Facts](#Facts)
* [Plays](#Plays)
* [Playbooks](#Playbooks)
* [Configuration files](#Configurationfiles)
* [Use provided documentation to look up specific information about Ansible modules and commands](#UseprovideddocumentationtolookupspecificinformationaboutAnsiblemodulesandcommands)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## <a name='Inventories'></a>Inventories

## <a name='Modules'></a>Modules

## <a name='Variables'></a>Variables

## <a name='Facts'></a>Facts

## <a name='Plays'></a>Plays

## <a name='Playbooks'></a>Playbooks

## <a name='Configurationfiles'></a>Configuration files

## <a name='UseprovideddocumentationtolookupspecificinformationaboutAnsiblemodulesandcommands'></a>Use provided documentation to look up specific information about Ansible modules and commands
