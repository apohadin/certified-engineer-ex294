# Be able to perform all tasks expected of a Red Hat Certified System Administrator

<!-- vscode-markdown-toc -->

* [Understand and use essential tools](#Understandanduseessentialtools)
* [Operate running systems](#Operaterunningsystems)
* [Configure local storage](#Configurelocalstorage)
* [Create and configure file systems](#Createandconfigurefilesystems)
* [Deploy, configure, and maintain systems](#Deployconfigureandmaintainsystems)
* [Manage users and groups](#Manageusersandgroups)
* [Manage security](#Managesecurity)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## <a name='Understandanduseessentialtools'></a>Understand and use essential tools

## <a name='Operaterunningsystems'></a>Operate running systems

## <a name='Configurelocalstorage'></a>Configure local storage

## <a name='Createandconfigurefilesystems'></a>Create and configure file systems

## <a name='Deployconfigureandmaintainsystems'></a>Deploy, configure, and maintain systems

## <a name='Manageusersandgroups'></a>Manage users and groups

## <a name='Managesecurity'></a>Manage security
