# Create Ansible plays and playbooks

<!-- vscode-markdown-toc -->

* [Know how to work with commonly used Ansible modules](#KnowhowtoworkwithcommonlyusedAnsiblemodules)
* [Use variables to retrieve the results of running a command](#Usevariablestoretrievetheresultsofrunningacommand)
* [Use conditionals to control play execution](#Useconditionalstocontrolplayexecution)
* [Configure error handling](#Configureerrorhandling)
* [Create playbooks to configure systems to a specified state](#Createplaybookstoconfiguresystemstoaspecifiedstate)

<!-- vscode-markdown-toc-config
	numbering=false
	autoSave=true
	/vscode-markdown-toc-config -->
<!-- /vscode-markdown-toc -->

## <a name='KnowhowtoworkwithcommonlyusedAnsiblemodules'></a>Know how to work with commonly used Ansible modules

## <a name='Usevariablestoretrievetheresultsofrunningacommand'></a>Use variables to retrieve the results of running a command

## <a name='Useconditionalstocontrolplayexecution'></a>Use conditionals to control play execution

## <a name='Configureerrorhandling'></a>Configure error handling

## <a name='Createplaybookstoconfiguresystemstoaspecifiedstate'></a>Create playbooks to configure systems to a specified state
